import requests


def load_points(file_path):
    points = []
    with open(file_path) as file:
        for line in file.readlines()[1:]:
            words = line.strip().split(",")
            points.append({"lat": float(words[2]), "lon": float(words[1])})
    print(f"success load points: {len(points)}")
    return points


def load_requests(points, count):
    requests_list = []
    for i in range(0, count):
        routing_data = {
            "alternative": 1,
            "locale": "ru",
            "type": "jam",
            "points": [
                {
                    "start": True,
                    "type": "pedo",
                    "x": points[i]['lon'],
                    "y": points[i]['lat']
                },
                {
                    "start": False,
                    "type": "pedo",
                    "x": points[i + 1]['lon'],
                    "y": points[i + 1]['lat']
                }
            ]
        }
        requests_list.append(routing_data)
    return requests_list


def run_load(url1, requests_list, url2, routes_count, file_path):
    count_prod_driving_direction = []
    for i, request in enumerate(requests_list):
        try:
            routing_resp = requests.post(url=url1, json=request)
            routing_resp.raise_for_status()
        except Exception as error:
            print(error)
            continue
        # count_of_dd = routing_resp.text.count("\"driving_direction\"")
        routing_json_resp = routing_resp.json()
        count_of_dd = len(routing_json_resp["result"])
        count_prod_driving_direction.append(count_of_dd)
        # print(count_of_dd)

    count_test_driving_direction = []
    for i, request in enumerate(requests_list):
        try:
            routing_resp = requests.post(url=url2, json=request)
            routing_resp.raise_for_status()
        except Exception as error:
            print(error)
            continue

        routing_json_resp = routing_resp.json()
        count_of_dd = len(routing_json_resp["result"])
        count_test_driving_direction.append(count_of_dd)
        # print(count_of_dd)

    print('на бою = ', count_prod_driving_direction)
    print('на тестовом = ', count_test_driving_direction)

    count_two_in_prod = count_prod_driving_direction.count(2)
    count_two_in_test = count_test_driving_direction.count(2)
    percent_of_two_prod = (count_two_in_prod / routes_count) * 100
    percent_of_two_test = (count_two_in_test / routes_count) * 100
    # print(count_two_in_test, count_two_in_prod)

    print(f'Процент маршрутов с альтернативами на боевом сервере составляет - {percent_of_two_prod} %')
    print(f'Процент маршрутов с альтернативами на тестовом сервере составляет - {percent_of_two_test} %')


def start(routes_count, file_path):
    url1 = "http://server_name/carrouting/3.0.0/novosibirsk"  # сборка эталон
    url2 = "http://server_name/carrouting/3.0.0/novosibirsk"  # сборка тестовая
    points = load_points(file_path)

    requests_list = load_requests(points, routes_count)
    run_load(url1, requests_list, url2, routes_count, file_path)
    #print(requests_list)


start(routes_count=10, file_path="points_moscow_2000m.csv")
